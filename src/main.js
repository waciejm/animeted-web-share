'use strict';

const FREEZE = 2
const IMAGES = 18
const FRAMES = 2 * (FREEZE + IMAGES)

function showImage(index) {
    document.getElementById("img" + index).style = "visibility: visible"
}

function hideImage(index) {
    document.getElementById("img" + index).style = "visibility: hidden"
}

function updateSeed(seed) {
    for (let i = 0; i < 18; ++i) {
        let url = "https://thisanimedoesnotexist.ai/results/psi-" + (0.3 + 0.1 * i).toFixed(1) + "/seed" + seed + ".png"
        document.getElementById("img" + i).setAttribute("src", url);
    }
}

function getParameter(key) {
    let parameterList = new URLSearchParams(window.location.search)
    return parameterList.get(key)
}

function imageFromFrame(frame) {
    if (frame < FREEZE) {
        return 0
    } else if (frame < FRAMES / 2) {
        return frame - FREEZE
    } else if (frame < FRAMES / 2 + FREEZE) {
        return 17
    } else {
        return FRAMES - frame - 1
    }
}


let current_frame = 0;
let fps = 20;

function animation_loop() {
    hideImage(imageFromFrame(current_frame));
    current_frame = (current_frame + 1) % FRAMES;
    showImage(imageFromFrame(current_frame));
    setTimeout(() => requestAnimationFrame(animation_loop), 1000 / fps);
};

function main() {
    let seed = getParameter("seed")
    updateSeed(seed)
    requestAnimationFrame(animation_loop);
}

window.onload = main;
